#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.elapsedtime import ElapsedTime
from PySketch.abstractflow import FlowChannel
from PySketch.flowsync import FlowSync
from PySketch.flowproto import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat import FlowSat

import os
import argparse
import tempfile
import numpy as np
import time
import json

#Force to not use cuda
# -> CUDA_VISIBLE_DEVICES="" ./whisperAsrDaemon.py --user Asr1

from pydub import AudioSegment

recording = False

args = None
sat = None

model = None
modelName = None

bufferFrames = 2048
channels = 1
rate = 22050

ctrlChanName = None
ctrl = None

pcmChanName = None
pcm = None

out = None
outChanName = None

currentBuffer = bytearray()

parser = argparse.ArgumentParser(description="ASR Whisper daemon")
parser.add_argument('sketchfile', help='Sketch program file')
parser.add_argument('--user', help='Flow-network username', default='User1')
parser.add_argument('--password', help='Flow-network password', default='password')
parser.add_argument('--model', help='Whisper model', default='tiny')
parser.add_argument('--language', help='Whisper language', default='Italian')
parser.add_argument('--ctrl-chan', help='Control channel', default='Agent.Ctrl')

###############################################################################
# SKETCH

def setup() -> bool:
    global parser
    global model
    global modelName
    global sat
    global pcmChanName
    global outChanName
    global ctrlChanName
    global args

    args = parser.parse_args()

    sat = FlowSat()
    sat._userName = args.user
    sat._passwd = args.password

    ##
    loadWhisperModel()
    ##

    ok: bool = sat.connect()

    sat.setNewChanCallBack(onChannelAdded)
    sat.setDelChanCallBack(onChannelRemoved)
    sat.setGrabDataCallBack(onDataGrabbed)

    ctrlChanName = f"{args.ctrl_chan}" #COULD BE A SERVICE CHANNEL TO RECEIVE CMDs
    pcmChanName = f"{sat._userName}.PcmInput"
    outChanName = f"{sat._userName}.Transcription"
    
    if ok:
        setIoChannels()

    return True

def loop() -> bool:
    global sat

    sat.tick()

    if not sat.isConnected():

        print("Triyng connection")

        if sat.connect():
            setIoChannels()
        else:
            time.sleep(1)

    return True

###############################################################################
# I/O

def setIoChannels():
    print("Creating I/O channels ..")
    sat.addStreamingChannel(Flow_T.FT_AUDIO_DATA, Variant_T.T_INT16, "PcmInput")
    sat.addStreamingChannel(Flow_T.FT_SPEECH_TO_TEXT, Variant_T.T_STRING, "Transcription", "application/json")

###############################################################################
# CALLBACKs

def onChannelAdded(ch: FlowChannel):
    global sat
    global recording
    global ctrlChanName
    global ctrl
    global pcmChanName
    global pcm
    global outChanName
    global out

    if ch.name == ctrlChanName:
        ctrl = ch
        sat.subscribeChannel(ctrl.chanID)
        print(f"Ctrl channel is READY: {ctrlChanName}")

    elif ch.name == pcmChanName:
        pcm = ch
        sat.subscribeChannel(pcm.chanID)
        print(f"Input channel is READY: {pcmChanName}")
        
    elif ch.name == outChanName:
        out = ch
        print(f"Output channel is READY: {outChanName}")

def onChannelRemoved(ch: FlowChannel):
    if ch.name == ctrlChanName:
        print(f"Ctrl channel is DOWN: {ctrlChanName}")

        if recording:
            terminate()
            
        ctrl = None

def onDataGrabbed(chanID: FlowChanID, data: bytearray):
    global recording
    global sat
    global ctrl
    global pcm
    global currentBuffer

    if ctrl is None:
        return

    if chanID == ctrl.chanID:
        jsonStr = data.decode('utf-8')
        cmdPck = json.loads(jsonStr)
        print(f"Ctrl switch: {cmdPck}")

        if cmdPck["target"] != sat._userName:
            return

        if cmdPck["cmd"] == "START":
            start()

        elif cmdPck["cmd"] == "STOP":
            stop()

        return

    elif recording and chanID == pcm.chanID:
        currentBuffer += data

###############################################################################
# WHISPER INIT

def loadWhisperModel():
    global model
    global args

    import torch
    print("PyTorch Version:", torch.__version__)

    import whisper
    print("Whisper Version:", whisper.__version__)

    device = "cpu";
    
    if torch.cuda.is_available():
        device = "cuda"
        torch.cuda.init()

    if device == "cpu":
        #if torch.cuda.is_available():
        print("WARNING: Performing inference on CPU when CUDA is available")
    
    print(f"Loading whisper model '{args.model}' [device: {device}; cuda-ver: {torch.version.cuda}] ...")
    
    chrono = ElapsedTime()
    chrono.start()
    model = whisper.load_model(args.model, device=device)
    print(f"LOAD-MODEL Time -> {chrono.stop()}")
    
###############################################################################
# RECORDING

def start():
    global recording
    recording = True
    print("Recording STARTED")

def stop():
    global sat
    global recording
    global out
    global currentBuffer
    global channels
    global rate
    global args

    audio = AudioSegment(
        currentBuffer,
        sample_width=2,
        frame_rate=rate,
        channels=channels
    )

    print(f"Recording FINISHED: {len(currentBuffer)} B, {audio.duration_seconds} s")
    
    totalFilePath = f"{tempfile.mktemp()}.wav"
    audio.export(totalFilePath, format="wav")

    #
    print(f"Transcripting ...")
    transcribingChrono = ElapsedTime()
    transcribingChrono.start()
    result = model.transcribe(totalFilePath, verbose=False, language=args.language, fp16=True)
    #result = model.transcribe(totalFilePath, language=args.language,fp16=False, word_timestamps=False, verbose=True)
    txt = result["text"]
    print(f"TRANSCRIPTION [clip-duration: {audio.duration_seconds} s; prcessing-time: {transcribingChrono.stop()}]: {txt}")
    #

    sat.publishJSON(out.chanID, result)
    
    os.unlink(totalFilePath)
    currentBuffer = bytearray()
    recording = False
    print(f"Capture session stopped")

def terminate():
    currentBuffer = bytearray()
    recording = False
    print(f"Capture session terminated")

###############################################################################
